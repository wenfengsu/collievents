//
//  Event.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject


@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *venueName;
@property (nonatomic, readonly) NSString *venueAddress;
@property (nonatomic, readonly) NSString *eventDescription;
@property (nonatomic, readonly) NSString *latitude;
@property (nonatomic, readonly) NSString *longitude;

@property (nonatomic, readonly) NSDate *startDate;
@property (nonatomic, readonly) NSDate *stopDate;

@property (nonatomic, readonly) NSURL *eventURL;

@property (nonatomic, readonly) NSDictionary *images;


@property (nonatomic, readonly) NSString *startDateStr;
@property (nonatomic, readonly) NSString *endDateStr;

@property (nonatomic, readonly) NSString *owner;


-(id) initWithEventData:(NSDictionary *)eventData;

@end
