//
//  EventTableDataSource.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "EventDataSourceInterface.h"
#import "ColliEventsManagerDelegate.h"
#import "ColliEventsIconRequesterDelegate.h"

@class IconStore;
@class ColliEventsConfigurator;
@class ColliEventsManager;

extern NSString *EventTableDidSelectEventNotification;

@interface EventTableDataSource : NSObject <UITableViewDelegate, UITableViewDataSource, EventDataSourceInterface, ColliEventsManagerDelegate>

@end
