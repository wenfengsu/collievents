//
//  EventBuilder.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "EventBuilder.h"
#import "Event.h"

@implementation EventBuilder


+ (NSArray *) eventsFromJSON: (NSData *) eventData{
    NSParameterAssert(eventData != nil);
    NSError *error = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:eventData options:kNilOptions error:&error];
    
    if (error) {
        NSLog(@"Parsing event data error: %@", error);
        return nil;
    }
    
    NSMutableArray *events = [[NSMutableArray alloc] init];
    
    NSArray *eventsJson = [[parsedObject valueForKey:@"events"] valueForKey:@"event"];
    
    if ([eventsJson count] <= 0) return nil;
    
    for (NSDictionary *eventDic in eventsJson) {
        Event *event = [[Event alloc] initWithEventData:eventDic];
        [events addObject:event];
        
    }
    
   return events;
    
}

@end
