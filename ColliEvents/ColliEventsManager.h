//
//  ColliEventsManager.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ColliEventsManagerDelegate.h"
#import "ColliEventsRequesterDelegate.h"

@class ColliEventsRequester;

@interface ColliEventsManager : NSObject <ColliEventsRequesterDelegate>
@property (nonatomic, weak) id <ColliEventsManagerDelegate> delegate;
@property (nonatomic, strong) ColliEventsRequester *eventsRequester;


-(void) fetchEventsWithParams:(NSDictionary *)params;
-(void) fetchEventsWithParams:(NSDictionary *)params
                 errorHandler: (void(^) (NSError *error)) errorBlock
               successHanlder: (void(^)(NSData *data)) successBlock;

@end
