//
//  SearchDateInputTableViewCell.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-26.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "SearchDateInputTableViewCell.h"

@implementation SearchDateInputTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
