//
//  ColliEventsManagerDelegate.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ColliEventsManagerDelegate <NSObject>



/**
 * Event manager receives a response from eventful
 */
-(void) managerFetchEventsDidReceiveEvents:(NSArray *) events;


/**
 * Event Manager failed to request events from eventful
 */
-(void) managerFetchEventsDidFinishedWithError:(NSError *) error;

@end
