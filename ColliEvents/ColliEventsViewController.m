//
//  ColliEventsViewController.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "ColliEventsViewController.h"

#import "ColliEventsSearchViewController.h"
#import "ColliEventsManager.h"
#import "EventTableDataSource.h"
#import "Event.h"
#import <objc/runtime.h>

@interface ColliEventsViewController ()
@end

@implementation ColliEventsViewController


@synthesize tableView;
@synthesize dataSource;
@synthesize configurator;
@synthesize eventManager;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self.dataSource;
    self.tableView.dataSource = self.dataSource;
    
    // we get the dataSouce type at runtime. This is the benefit of dynamic typing, we can find out the actual dataSource type at runtime.
    objc_property_t tableViewProperty = class_getProperty([dataSource class], "tableView");
    if (tableViewProperty) {
        [dataSource setValue: tableView forKey: @"tableView"];
    }
    
    /***********************************
     This is the O in SOLID: Open-Closed Principle
     1. Create an protocol/interface for DataSource: In order to be a valid datasource for this viewController, all DataSource classes should conform to this protocol
     2. Each datasource class must implement the following methods
     
     -(void) setupDataSourceforTableView:(UITableView *)tableView configurator:(ColliEventsConfigurator *)configurator;
     *******************************************************/
    
    [self.dataSource setupDataSourceforTableView:self.tableView configurator:configurator];
    

}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if ([self.dataSource respondsToSelector:@selector(removeObserverForInterestedEventsWithNotificationCenter:)]) {
        [self.dataSource removeObserverForInterestedEventsWithNotificationCenter:[NSNotificationCenter defaultCenter]];
    }
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    

    if ([self.dataSource respondsToSelector:@selector(addObserverForInsterestedEventsWithNotificationCenter:)]) {
        [self.dataSource addObserverForInsterestedEventsWithNotificationCenter:[NSNotificationCenter defaultCenter]];
    }
    
    // listen to EventTableDidSelectEventNotification from EventTableDataSource 
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(userDidSelectEventNotification:)
     name: EventTableDidSelectEventNotification
     object: nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)filterButtonClicked:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ColliEventsSearchViewController *searchViewController = [storyboard instantiateViewControllerWithIdentifier:@"EventsSearchViewController"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - Notification handling

- (void)userDidSelectEventNotification: (NSNotification *)note {
    // we will push to a event detail view
    NSLog(@"user selected a cell");
    //Event * selectedEvent = (Event *)[note object];
    //NSLog(@"pushDetailViewController with %@", selectedEvent);

}


@end
