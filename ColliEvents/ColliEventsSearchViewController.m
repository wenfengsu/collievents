//
//  ColliEventsSearchViewController.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "ColliEventsSearchViewController.h"
#import "ColliEventsSearchViewDataSource.h"

@implementation ColliEventsSearchViewController

@synthesize tableView;
@synthesize dataSource;


-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.dataSource = [ColliEventsSearchViewDataSource new];
    [self.dataSource setupTableSource];
    
    self.tableView.delegate = self.dataSource;
    self.tableView.dataSource = self.dataSource;
    
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonClicked)];
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonClicked)];
    
    self.navigationItem.leftBarButtonItem = cancelButton;
    self.navigationItem.rightBarButtonItem = searchButton;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
}


-(void)cancelButtonClicked{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)doneButtonClicked{
    [self dismissViewControllerAnimated:YES completion:^{
        //We will initiate the event search here
        NSLog(@"search start");
    }];
}




@end
