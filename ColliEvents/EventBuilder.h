//
//  EventBuilder.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Event;
@interface EventBuilder : NSObject

+ (NSArray *) eventsFromJSON: (NSData *) eventData;

@end
