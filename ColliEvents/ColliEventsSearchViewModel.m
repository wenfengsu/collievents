//
//  ColliEventsSearchViewModel.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "ColliEventsSearchViewModel.h"
#import "AppDelegate.h"

@interface ColliEventsSearchViewModel()


@property(nonatomic) NSString *address;

@property(nonatomic) NSString *radius;

@property(nonatomic) NSString *startDateDay;
@property(nonatomic) NSString *startDateMonth;
@property(nonatomic) NSString *startDateYear;

@property(nonatomic) NSString *endDateDay;
@property(nonatomic) NSString *endDateMonth;
@property(nonatomic) NSString *endDateYear;

@property(nonatomic) NSString *category;

@property (nonatomic) NSDate *startDate;
@property (nonatomic) NSDate *endDate;

@property (nonatomic) NSManagedObjectContext *context;

@end


@implementation ColliEventsSearchViewModel

@synthesize searchCriterias;


-(id)init{
    
    self = [super init];
    if (!self) {
        return nil;
    }
    
    [self initialize];
    return self;
    
}

- (void)initialize{
    
    self.context = [self managedObjectContext];
    searchCriterias = [self getSearchCriterias];
    if (!searchCriterias) {
        [self setDefaultSearchCriterias];
    }
    
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(SearchCriterias *)getSearchCriterias{
    //NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SearchCriterias" inManagedObjectContext:self.context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = entity;
    request.fetchLimit = 1;
    
    NSError *error;
    NSArray *fetchResults = [self.context executeFetchRequest:request error:&error];
    
    // we can check if there is error, but we don't worry about here, if it's nil, we will create some default search criterias
    return fetchResults.firstObject;
}


-(void)resetSearchCriterias{
    
    if (searchCriterias) {
        [self.context deleteObject:searchCriterias];
    }
    
    [self setDefaultSearchCriterias];
}

-(void)setDefaultSearchCriterias{
    
    // Create a new search criterias
    searchCriterias = [NSEntityDescription insertNewObjectForEntityForName:@"SearchCriterias" inManagedObjectContext:self.context];
    
    NSDate *today = [NSDate date];
    [self setStartDate:today];
    [searchCriterias setValue:today forKey:@"startDate"];
    [searchCriterias setValue:_endDate forKey:@"endDate"];
    [searchCriterias setValue:@"" forKey:@"address"];
    [searchCriterias setValue:@1 forKey:@"radius"];
    [searchCriterias setValue:@"Music" forKey:@"category"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![self.context save:&error]) {
        NSLog(@"Save failed: %@ %@", error, [error localizedDescription]);
    }
    
}

-(void)saveSearchCriterias{
    
    if (searchCriterias) {
        // Update existing device
        [searchCriterias setValue:self.address forKey:@"address"];
        [searchCriterias setValue:self.startDate forKey:@"startDate"];
        [searchCriterias setValue:_endDate forKey:@"endDate"];
        [searchCriterias setValue:[NSNumber numberWithFloat:[self.radius floatValue]] forKey:@"radius"];
        [searchCriterias setValue:self.category forKey:@"category"];
    }
    NSError *error = nil;
    // Save the object to persistent store
    if (![self.context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}




-(void)setStartDate:(NSDate *)mStartDate{
    
    _startDate = mStartDate;
    
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd"];
    self.startDateDay = [df stringFromDate:self.startDate];
    
    [df setDateFormat:@"MMM"];
    self.startDateMonth = [df stringFromDate:self.startDate];
    
    [df setDateFormat:@"yyyy"];
    self.startDateYear = [df stringFromDate:self.startDate];
    
    [self setEndDateAfterStartDate:_startDate];
}

- (void) setEndDateAfterStartDate:(NSDate *)mStartDate{
    
    
    _endDate = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:mStartDate];
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd"];
    self.endDateDay = [df stringFromDate:self.endDate];;
    
    [df setDateFormat:@"MMM"];
    self.endDateMonth = [df stringFromDate:self.endDate];
    
    [df setDateFormat:@"yyyy"];
    self.endDateYear = [df stringFromDate:self.endDate];
    
    
    
    
}

-(BOOL)validateStartDateEntered:(NSString *)dayInput monthInput:(NSString *)monthInput yearInput:(NSString *)yearInput{
    
    NSDate *tmpStartDate = [self convertDateFromStartDay:dayInput month:monthInput year:yearInput];
    
    if (!tmpStartDate) {
        
        return NO;
    }
    
    // if start date is valid, we change the end date one day after
    _startDate = tmpStartDate;
    self.startDateDay = dayInput;
    self.startDateMonth = monthInput;
    self.startDateYear = yearInput;
    
    [self setStartDate:self.startDate];
    [self saveSearchCriterias];
    
    return YES;
    
}

-(BOOL)validateEndDateEndtered:(NSString *)dayInput monthInput:(NSString *)monthInput yearInput:(NSString *)yearInput{
    
    if ([dayInput intValue] <= 0 || [monthInput intValue] <= 0 || [yearInput intValue] <= 0) {
        return NO;
    }
    
    NSDate *tmpEndDate = [self convertDateFromStartDay:dayInput month:monthInput year:yearInput];
    
    if (!tmpEndDate) {
        // not a valid date. ie. 22/33/0000
        return NO;
    }
    
    if ([self.startDate earlierDate:tmpEndDate] == tmpEndDate)  return NO;
    
    _endDate = tmpEndDate;
    self.endDateDay = dayInput;
    self.endDateMonth = monthInput;
    self.endDateYear = yearInput;
    [self saveSearchCriterias];
    
    return YES;
    
}

-(BOOL)validateRadius:(NSString *)radiusInput{
    
    // to check if it is a valid numeric input
    if (![radiusInput floatValue]) return NO;
    
    // to check if the input is greater than 1, and smaller than 300
    if (!([radiusInput floatValue] >= 1) && !([radiusInput floatValue] <= 300))  return NO;
    
    self.radius = radiusInput;
    
    [self saveSearchCriterias];
    return YES;
    
}

-(NSDate *) convertDateFromStartDay:(NSString *)day month:(NSString *)month year:(NSString *)year{
    
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd/MM/yyyy"];
    NSDate *date = [df dateFromString:[NSString stringWithFormat:@"%@/%@/%@", day, month, year]];
    
    return date;
}


@end
