//
//  ColliEventConfigurator.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-24.
//  Copyright © 2016 Edmund Su. All rights reserved.
//


/**
  Classes passe a configurator around through out the application life cycle. 
  Whenever additional shared functions or features needed, we can add them here and this will increase the flexibility.
 
  The configurator doesn't do anything except carrying around the Event Manager and Icon Storage.
 */

#import "ColliEventsConfigurator.h"
#import "ColliEventsManager.h"
#import "IconStore.h"
#import "ColliEventsRequester.h"

@implementation ColliEventsConfigurator


-(ColliEventsManager *)colliEventsManager{
    ColliEventsManager *manager = [[ColliEventsManager alloc] init];
    manager.eventsRequester = [[ColliEventsRequester alloc] init];
    manager.eventsRequester.delegate = manager;
    return manager;
}

-(IconStore *)iconStore{
    static IconStore *iconStore = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        iconStore = [[IconStore alloc] init];
        [iconStore startMemoryWarningNotificationWithNotificationCeter:[NSNotificationCenter defaultCenter]];
    });
    return iconStore;
    
    
}

@end
