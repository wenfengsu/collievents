//
//  ColliEventsManager.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "ColliEventsManager.h"
#import "ColliEventsRequester.h"
#import "EventBuilder.h"

@implementation ColliEventsManager

@synthesize delegate;
@synthesize eventsRequester;


- (void) fetchEventsWithParams:(NSDictionary *)params{
    [eventsRequester searchEventWithParamers:params errorHandler:nil successHanlder:nil];
}

- (void) fetchEventsWithParams:(NSDictionary *)params errorHandler:(void (^)(NSError *))errorBlock successHanlder:(void (^)(NSData *))successBlock{
    [eventsRequester searchEventWithParamers:params errorHandler:errorBlock successHanlder:successBlock];
}

#pragma mark - EventsHttpReuqester Delegate

- (void) searchEventDidFinishedWithResult:(NSData *) eventsData{
    NSArray *events  = [EventBuilder eventsFromJSON:eventsData];
    [delegate managerFetchEventsDidReceiveEvents:events];
    
}

- (void) searchEventDidFinishedWithError:(NSError *) error{
    [delegate managerFetchEventsDidFinishedWithError:error];
}

@end
