//
//  ColliEventsSearchViewDataSource.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-23.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "ColliEventsSearchViewDataSource.h"

NSString *searchCriteriasCellReuseIdentifier = @"SearchCell";


@interface ColliEventsSearchViewDataSource()

@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) NSArray *fields;
@property (nonatomic, strong) ColliEventsSearchViewModel *searchViewModel;


@end

@implementation ColliEventsSearchViewDataSource{
    
}

@synthesize searchViewModel;

-(void)setupTableSource{
    
    self.searchViewModel = [ColliEventsSearchViewModel new];
    
    
    self.sections = @[@"Address", @"Start Date", @"End Date", @"Radius", @"Category"];
    
    UITextView *addressTextView = [UITextView new];
    UITextField *startDateField = [UITextField new];
    UITextField *endDateField = [UITextField new];
    UIDatePicker *startDatePicker = [UIDatePicker new];
    UIDatePicker *endDatePicker = [UIDatePicker new];
    UITextField *radiusField = [UITextField new];
    UITextField *categoryField = [UITextField new];
    
    UIView *startDateView = [[UIView alloc] init];
    [startDateView addSubview:startDateField];
    [startDateView addSubview:startDatePicker];
    
    self.fields = @[addressTextView, @[startDateField, startDatePicker], @[endDateField, endDatePicker], radiusField, categoryField];

}



#pragma mark - UITabelView Delegate

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.fields count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    id cellData = [self.fields objectAtIndex:section];
    if ([cellData isKindOfClass:[NSArray class]]) {
        return [cellData count];
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *searchCell = [tableView dequeueReusableCellWithIdentifier:searchCriteriasCellReuseIdentifier forIndexPath:indexPath];
    searchCell.textLabel.text = @"test search cell";
    
    return searchCell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [self.sections objectAtIndex:section];
}

#pragma mark - UITextView Delegate
- (void)textViewDidEndEditing:(UITextView *)textView{
    // we will validate the address entered with google geocode
}

#pragma mark - UITextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    // we will validate the user input here
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    // we will limit the number of digits allowed to be entered
    return YES;
}

@end
