//
//  ColliEventConfigurator.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-24.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>


@class ColliEventsManager;
@class IconStore;

@interface ColliEventsConfigurator : NSObject


-(ColliEventsManager *)colliEventsManager;

-(IconStore *)iconStore;

@end
