//
//  ColliEventsSearchViewModel.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SearchCriterias.h"

@interface ColliEventsSearchViewModel : NSObject

@property(nonatomic) SearchCriterias *searchCriterias;


-(BOOL)validateStartDateEntered:(NSString *)dayInput monthInput:(NSString *)monthInput yearInput:(NSString *)yearInput;
-(BOOL)validateEndDateEndtered:(NSString *)dayInput monthInput:(NSString *)monthInput yearInput:(NSString *)yearInput;
-(BOOL)validateRadius:(NSString *)radiusInput;
-(void)resetSearchCriterias;

@end
