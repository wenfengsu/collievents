//
//  SearchCriterias.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-22.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchCriterias : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "SearchCriterias+CoreDataProperties.h"
