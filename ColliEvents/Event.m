//
//  Event.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "Event.h"

@interface Event()

@property (nonatomic, readwrite) NSString *title;
@property (nonatomic, readwrite) NSString *venueName;
@property (nonatomic, readwrite) NSString *venueAddress;
@property (nonatomic, readwrite) NSString *eventDescription;

@property (nonatomic, readwrite) NSString *latitude;
@property (nonatomic, readwrite) NSString *longitude;

@property (nonatomic, readwrite) NSDate *startDate;
@property (nonatomic, readwrite) NSDate *stopDate;

@property (nonatomic, readwrite) NSURL *eventURL;

@property (nonatomic, readwrite) NSDictionary *images;


@property (nonatomic, readwrite) NSString *startDateStr;
@property (nonatomic, readwrite) NSString *endDateStr;

@property (nonatomic, readwrite) NSString *owner;

@end


@implementation Event


-(id) initWithEventData:(NSDictionary *)eventData{
    if(self = [super init]){
        self.title = [eventData valueForKey:@"title"];
        self.venueName = [eventData valueForKey:@"venue_name"];
        self.venueAddress = [eventData valueForKey:@"venue_address"];
        
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
        self.startDate = [df dateFromString: [eventData valueForKey:@"start_time"]];
        
        if (![[eventData valueForKey:@"stop_time"] isEqual:[NSNull null]]) {
            self.stopDate = [df dateFromString: [eventData valueForKey:@"stop_time"]];
        }
        
        NSDateFormatter *df2 = [NSDateFormatter new];
        [df2 setDateFormat: @"MM/dd/yy, EEEE"];
        
        self.startDateStr = [df2 stringFromDate: self.startDate];
        if (self.stopDate) {
            self.endDateStr = [df2 stringFromDate:self.stopDate];
        }
        
        self.owner = [eventData valueForKey:@"owner"];
        self.latitude = [eventData valueForKey:@"latitude"];
        self.longitude = [eventData valueForKey:@"longitude"];
        self.eventURL = [NSURL URLWithString:[eventData valueForKey:@"url"]];
        
        
        if ([eventData objectForKey:@"image"]) {
            self.images = [eventData objectForKey:@"image"];
            
        }else{
            self.images = nil;
        }
        
    }
    
    return self;
}

@end
