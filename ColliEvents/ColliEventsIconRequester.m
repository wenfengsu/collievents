//
//  ColliEventIconRequester.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-23.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "ColliEventsIconRequester.h"

@implementation ColliEventIconRequester 

@synthesize delegate;


- (void)fetchIconDataFromURL: (NSURL *)url{
    [self fetchIconDataFromURL:url errorHandler:nil successHanlder:nil];
}

- (void)fetchIconDataFromURL: (NSURL *)url errorHandler: (void(^) (NSError *error)) errorBlock successHanlder: (void(^)(NSData *data)) successBlock{
    
    fetchingURL = url;
    errorHandler = [errorBlock copy];
    successHandler = [successBlock copy];
    
  
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    downloadTask = [session downloadTaskWithURL:url
                              completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                       
                                  if (error) {
                                        if (errorHandler) {
                                          errorHandler(error);
                                        }
                                                           
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [delegate iconRequestDidFailedWithError:error fromURL:location];
                                                               
                                            });
                                                           
                                  }else{
                                        NSData *imageData = [NSData dataWithContentsOfURL:location];
                                      
                                      
                                      
                                        if (successHandler) {
                                            successHandler(imageData);
                                        }
                                                           
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [delegate iconRequestDidReceivedData:imageData fromURL:url];
                                        });
                                                           
                                  }
                    }];
    
    
    [downloadTask resume];
}

-(void) cancelDownloadTask{
    [downloadTask cancel];
    downloadTask = nil;
}


@end
