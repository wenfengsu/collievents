//
//  SearchCriterias+CoreDataProperties.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-22.
//  Copyright © 2016 Edmund Su. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SearchCriterias+CoreDataProperties.h"

@implementation SearchCriterias (CoreDataProperties)

@dynamic address;
@dynamic startDate;
@dynamic endDate;
@dynamic radius;
@dynamic category;

@end
