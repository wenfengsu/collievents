//
//  EventDataSourceInterface.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-25.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/Uikit.h>

@class ColliEventsConfigurator;
@class IconStore;

@protocol EventDataSourceInterface <NSObject>

@required
-(void) setupDataSourceforTableView:(UITableView *)tableView configurator:(ColliEventsConfigurator *)configurator;

/*
 
 we should further extract these to other protocol, so base on viewController's need, they can conform to different protocol.
 This is the I in SOLID : Interface segregation principle
 
 @protocol EventDataSourceInterfaceEventsViewController
 @protocol EventDataSourceInterfaceSearchEventViewController
 
 */
@optional
- (void) addObserverForInsterestedEventsWithNotificationCenter:(NSNotificationCenter *) notificationCenter;
- (void) removeObserverForInterestedEventsWithNotificationCenter:(NSNotificationCenter *) notificationCenter;

@end
