//
//  ColliEventsRequester.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "ColliEventsRequester.h"

NSString *EventsRequesterErrorDomain = @"EventsRequesterErrorDomain";
#define SEARCH_URL "http://api.eventful.com/json/events/search?app_key=hmCJHTTp2pqXHcsj"
/********** Eventful api ************
 app_key:       hmCJHTTp2pqXHcsj
 date:          Ranges can be specified the form 'YYYYMMDD00-YYYYMMDD00', for example '2012042500-2012042700';
 the last two digits of each date in this format are ignored. (optional)
 
 c:             category
 within:        10
 units:         km   -- default is mi
 where:         32.746682,-117.162741 - (lat, long)
 **************************************/

@interface ColliEventsRequester()

- (void)fetchContentFromURL: (NSURL *)url errorHandler: (void(^) (NSError *error)) errorBlock successHanlder: (void(^)(NSData *data)) successBlock;
- (void)launchRequest;

@end

@implementation ColliEventsRequester

@synthesize delegate;

- (void) launchRequest{
    
    // we want to cancel the same request if it is already running
    [self cancelDataTask];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:fetchingURL];
    request.HTTPMethod = @"GET";
    
    dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error) {
            if (errorHandler) {
                errorHandler(error);
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate searchEventDidFinishedWithError:error];
                
            });
            
        }else{
            if (successHandler) {
                successHandler(data);
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate searchEventDidFinishedWithResult:data];
    
            });
            
        }
    }];
    
    [dataTask resume];
}

-(void)fetchContentFromURL: (NSURL *)url errorHandler: (void(^) (NSError *error)) errorBlock successHanlder: (void(^)(NSData *data)) successBlock{
    
    fetchingURL = url;
    errorHandler = [errorBlock copy];
    successHandler = [successBlock copy];
    
    [self launchRequest];
    
}


- (void)searchEventWithParamers:(NSDictionary *)parameters
                   errorHandler: (void(^) (NSError *error)) errorBlock
                 successHanlder: (void(^)(NSData *data)) successBlock{
    
    NSString *paramsStr = NSStringFromQueryParameters(parameters);
    
    NSString *urlStr = [NSString stringWithFormat:@"%s&%@", SEARCH_URL, paramsStr];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    
    [self fetchContentFromURL:url errorHandler:errorBlock successHanlder:successBlock];

    
}

- (void)cancelDataTask{
    [dataTask cancel];
    dataTask = nil;
}

/**
 This creates a new query parameters string from the given NSDictionary.
 @return The created parameters string.
 */
static NSString* NSStringFromQueryParameters(NSDictionary* queryParameters)
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:queryParameters];
    
    NSMutableArray* parts = [NSMutableArray array];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
        NSString *part = [NSString stringWithFormat: @"%@=%@",
                          [key stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]],
                          [value stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]
                          ];
        [parts addObject:part];
    }];
    
    return [parts componentsJoinedByString: @"&"];
}

@end
