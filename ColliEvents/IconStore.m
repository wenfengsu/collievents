//
//  IconStore.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-24.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "IconStore.h"
#import "ColliEventsIconRequester.h"

NSString *IconStoreDidUpdateContentNotification = @"IconStoreDidUpdateContentNotification";

@implementation IconStore


-(id) init{
    
    self = [super init];
    if (self){
        dataCache = [[NSMutableDictionary alloc] init];
        requesters = [[NSMutableDictionary alloc] init];
    }
    return self;
    
}

- (NSData *) dataForURL: (NSURL *)url{
    if ([url isEqual:[NSNull null]] || !url ) {
        return nil;
    }
    
    //NSURL *url = [NSURL URLWithString:urlStr];
    NSData *iconData = [dataCache objectForKey:[url absoluteString]];
    if (!iconData) {
        ColliEventIconRequester *requester = [ColliEventIconRequester new];
        [requesters setObject:requester forKey:[url absoluteString]];
        requester.delegate = self;
        [requester fetchIconDataFromURL:url];
        
    }
    return iconData;
}

#pragma mark - IconRequester delegate

/*
 We take notification center as an argument because we want to this the notification center test friendly, Please see FakeNotificationCenter.h/m
 */

- (void)iconRequestDidReceivedData: (NSData *)data fromURL: (NSURL *)url{
    
    [dataCache setObject:data forKey:[url absoluteString]];
    [requesters removeObjectForKey:[url absoluteString]];
    NSNotification *notification = [NSNotification notificationWithName:IconStoreDidUpdateContentNotification object:self];
    [notificationCenter postNotification:notification];
    
}

- (void)iconRequestDidFailedWithError: (NSError *)error fromURL:(NSURL *)url{
    [requesters removeObjectForKey: [url absoluteString]];
    
}

#pragma mark - Memory Management

- (void) didReceiveMemoryWarning: (NSNotification *) note{
    [dataCache removeAllObjects];
}

-(void) startMemoryWarningNotificationWithNotificationCeter: (NSNotificationCenter *)center{
    [center addObserver: self selector: @selector(didReceiveMemoryWarning:) name: UIApplicationDidReceiveMemoryWarningNotification object: nil];
    notificationCenter = center;
}

-(void) stopMemoryWarningNotificationWithNotificationCenter: (NSNotificationCenter *)center{
    [center removeObserver:self];
    notificationCenter = nil;
}


@end
