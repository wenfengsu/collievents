//
//  AddressVerificationRequester.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>



#define GEOCODE_URL "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCtG2Hk-3NzNaj7hra-4SV6VFDsfAr1Lt4"
/************** Google Geocode api************
 key:           AIzaSyCtG2Hk-3NzNaj7hra-4SV6VFDsfAr1Lt4
 address:       the full address
 *********************************************/

@interface AddressVerificationRequester : NSObject

@end
