//
//  ColliEventsViewController.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColliEventsConfigurator.h"
#import "EventDataSourceInterface.h"

@class ColliEventsConfigurator;
@class ColliEventsManager;

@interface ColliEventsViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSObject <UITableViewDataSource, UITableViewDelegate, EventDataSourceInterface> *dataSource;

@property (nonatomic, strong) ColliEventsManager *eventManager;
@property (nonatomic, strong) ColliEventsConfigurator *configurator;

//@property (nonatomic, strong) ColliEventsConfigurator *configurator;

@end
