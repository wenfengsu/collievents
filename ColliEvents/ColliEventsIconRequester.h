//
//  ColliEventIconRequester.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-23.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColliEventsIconRequesterDelegate.h"

@interface ColliEventIconRequester : NSObject{
    
    
    NSURL *fetchingURL;
    NSURLSessionDownloadTask *downloadTask;
    
    void (^errorHandler)(NSError *);
    void (^successHandler)(NSData *);
}


@property (nonatomic, weak) id <ColliEventsIconRequesterDelegate> delegate;

- (void)fetchIconDataFromURL: (NSURL *)url;
- (void)fetchIconDataFromURL: (NSURL *)url errorHandler: (void(^) (NSError *error)) errorBlock successHanlder: (void(^)(NSData *data)) successBlock;
- (void)cancelDownloadTask;
@end
