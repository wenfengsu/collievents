//
//  ColliEventsSearchViewController.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColliEventsSearchViewModel.h"
#import "ColliEventsSearchViewDataSource.h"

@interface ColliEventsSearchViewController : UIViewController


// the view only referneces it, but it does not own the viewModel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ColliEventsSearchViewDataSource <UITableViewDataSource, UITableViewDelegate> *dataSource;

@end
