//
//  EventTableDataSource.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "EventTableDataSource.h"
#import "Event.h"
#import "EventTableViewCell.h"
#import "IconStore.h"
#import "ColliEventsManager.h"
#import "ColliEventsConfigurator.h"

NSString *eventCellReuseIdentifier = @"EventCell";
NSString *EventTableDidSelectEventNotification = @"EventTableDidSelectEventNotification";

@interface EventTableDataSource(){
    UITableView *mTableView;
    IconStore *iconStore;
    NSMutableArray *events;
    ColliEventsManager *eventManager;
}

@end

@implementation EventTableDataSource

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return events.count;
}

- (EventTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EventTableViewCell *eventCell = [tableView dequeueReusableCellWithIdentifier:eventCellReuseIdentifier];
    if (!eventCell) {
        [tableView registerNib:[UINib nibWithNibName:@"EventTableViewCell" bundle:nil] forCellReuseIdentifier:eventCellReuseIdentifier];
        eventCell = (EventTableViewCell *)[tableView dequeueReusableCellWithIdentifier:eventCellReuseIdentifier];
    }
    
    
    Event *event = [events objectAtIndex:indexPath.row];
    eventCell.titleLabel.text = event.title;
    
    eventCell.startDateLabel.text = event.startDateStr;
    eventCell.ownerLabel.text = event.owner;
    eventCell.typeLabel.text = @"venue";
    
    
    // I don't like this, future improvement needed for
    if ([[[event.images valueForKey:@"thumb"] valueForKey:@"url"] isEqual:[NSNull null]]) {
        eventCell.thumbImageView.image = [UIImage imageNamed:@"event_placeholder.png"];
        
    }else{
        NSData *iconData = [iconStore dataForURL: [NSURL URLWithString:[[event.images valueForKey:@"thumb"] valueForKey:@"url"]]];
        if (iconData) {
            eventCell.thumbImageView.image = [UIImage imageWithData: iconData];
        }else{
            eventCell.thumbImageView.image = [UIImage imageNamed:@"event_placeholder.png"];
        }
    }
    /************************/
    
    
    
    
    return eventCell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSNotification *note = [NSNotification notificationWithName:EventTableDidSelectEventNotification object:[events objectAtIndex:indexPath.row]];
    [[NSNotificationCenter defaultCenter] postNotification:note];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.0f;
}


#pragma mark - EventDataSource Protocol
-(void) setupDataSourceforTableView:(UITableView *)tableView configurator:(ColliEventsConfigurator *)configurator{
    
    mTableView = tableView;
    eventManager = [configurator colliEventsManager];
    eventManager.delegate = self;
    
    iconStore = [configurator iconStore];
    
    // this is only placed here for testing purpose
    [self startFetchingEvents];
    
}

- (void) addObserverForInsterestedEventsWithNotificationCenter:(NSNotificationCenter *) notificationCenter{
    
    // we will listen to search filter changed notification for fetching new events base on the new fitler criterias
    [notificationCenter addObserver:self selector:@selector(startFetchingEvents)
                                            name:@"kSearchFilterChangedNotification"
                                            object:nil];
    
    [notificationCenter addObserver:self selector:@selector(iconStoreDidUpdateContent:) name:IconStoreDidUpdateContentNotification object:nil];
    
}

- (void) removeObserverForInterestedEventsWithNotificationCenter:(NSNotificationCenter *)notificationCenter{
    [notificationCenter removeObserver: self name: @"kSearchFilterChangedNotification" object: nil];
    [notificationCenter removeObserver:self name:IconStoreDidUpdateContentNotification object:iconStore];
    
}


#pragma mark - ColliEventsManager delegate
- (void)managerFetchEventsDidFinishedWithError:(NSError *)error{
    [events removeAllObjects];
    [mTableView reloadData];
    NSLog(@"Error: %@ - %@", error, [error localizedDescription]);
}

- (void)managerFetchEventsDidReceiveEvents:(NSArray *)mEvents{
    events = [mEvents mutableCopy];
    [mTableView reloadData];
}

#pragma mark - Private Methods

-(void) startFetchingEvents{
    
    // EventManager will get the params from searchCriterias, I hard coded these criteria here only for testing and development purppose.
    // This need to be removed and base on if there is any search criteria changes.
    NSDictionary *params = @{@"date": @"2016042500-2016042700", @"within": @"20", @"where": @"32.746682,-117.162741", @"units": @"km", @"c":@"Performing Arts"};
    
    [eventManager fetchEventsWithParams:params];
}

- (void)iconStoreDidUpdateContent: (NSNotification *)notification{
    [mTableView reloadData];
}


@end
