//
//  ColliEventsRequesterDelegate.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//


/**
 * Requester handle all the http request to eventful for event listins
 */
#import <Foundation/Foundation.h>

@protocol ColliEventsRequesterDelegate <NSObject>


/**
 * Requester receives a response from eventful
 */
-(void) searchEventDidFinishedWithResult:(NSData *) events;


/**
 * Requester failed to request events from eventful
 */
-(void) searchEventDidFinishedWithError:(NSError *) error;

@end
