//
//  IconStore.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-24.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ColliEventsIconRequesterDelegate.h"

extern NSString *IconStoreDidUpdateContentNotification;

@interface IconStore : NSObject <ColliEventsIconRequesterDelegate>{
    NSMutableDictionary *dataCache;
    NSMutableDictionary *requesters;
    NSNotificationCenter *notificationCenter;
}

- (NSData *) dataForURL: (NSURL *)url;
- (void) didReceiveMemoryWarning: (NSNotification *) note;

- (void) startMemoryWarningNotificationWithNotificationCeter: (NSNotificationCenter *)center;

- (void) stopMemoryWarningNotificationWithNotificationCenter: (NSNotificationCenter *)center;

@end
