//
//  ColliEventsSearchViewDataSource.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-23.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ColliEventsSearchViewModel.h"

@interface ColliEventsSearchViewDataSource : NSObject <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UITextFieldDelegate>

-(void)setupTableSource;

@end
