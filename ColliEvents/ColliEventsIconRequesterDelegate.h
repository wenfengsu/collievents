//
//  ColliEventIconRequesterDelegate.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-23.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ColliEventsIconRequesterDelegate <NSObject>

- (void)iconRequestDidReceivedData: (NSData *)data fromURL: (NSURL *)url;

- (void)iconRequestDidFailedWithError: (NSError *)error fromURL:(NSURL *)url;

@end
