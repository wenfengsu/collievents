//
//  ColliEventsRequester.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-20.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColliEventsRequesterDelegate.h"

extern NSString *EventRequesterErrorDomain;
@interface ColliEventsRequester : NSObject{
    
    NSURL *fetchingURL;
    NSURLSessionDataTask *dataTask;
    
    void (^errorHandler)(NSError *);
    void (^successHandler)(NSData *);
    
}

@property (nonatomic, weak) id <ColliEventsRequesterDelegate> delegate;

- (void)searchEventWithParamers:(NSDictionary *)parameters
                   errorHandler: (void(^) (NSError *error)) errorBlock
                 successHanlder: (void(^)(NSData *data)) successBlock;

- (void)cancelDataTask;


@end
