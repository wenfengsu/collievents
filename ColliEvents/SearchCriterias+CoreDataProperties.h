//
//  SearchCriterias+CoreDataProperties.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-22.
//  Copyright © 2016 Edmund Su. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SearchCriterias.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchCriterias (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address;
@property (nullable, nonatomic, retain) NSDate *startDate;
@property (nullable, nonatomic, retain) NSDate *endDate;
@property (nullable, nonatomic, retain) NSNumber *radius;
@property (nullable, nonatomic, retain) NSString *category;

@end

NS_ASSUME_NONNULL_END
