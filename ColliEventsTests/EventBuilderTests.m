//
//  EventTest.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-24.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Event.h"
#import "EventBuilder.h"

// this is a mockup item json file, it only contains one item.
static NSString *eventJSON =
@"{"
    @"\"last_item\":null,"
    @"\"total_items\":\"121835\","
    @"\"first_item\":null,"
    @"\"page_number\":\"1\","
    @"\"page_size\":\"10\","
    @"\"page_items\":null,"
    @"\"search_time\":\"0.044\","
    @"\"page_count\":\"12184\","
    @"\"events\":{"
        @"\"event\":["
@"{"
@"\"watching_count\":null,"
@"\"olson_path\":\"America/Chicago\","
@"\"calendar_count\":null,"
@"\"comment_count\":null,"
@"\"region_abbr\":\"TX\","
@"\"postal_code\":\"77032\","
@"\"going_count\":null,"
@"\"all_day\":\"0\","
@"\"latitude\":\"29.9865720\","
@"\"groups\":null,"
@"\"url\":\"http://houston.eventful.com/events/national-social-security-advisor-nssaworkshop-ho-/E0-001-079207392-0?utm_source=apis&utm_medium=apim&utm_campaign=apic\","
@"\"id\":\"E0-001-079207392-0\","
@"\"privacy\":\"1\","
@"\"city_name\":\"Houston\","
@"\"link_count\":null,"
@"\"longitude\":\"-95.3417160\","
@"\"country_name\":\"United States\","
@"\"country_abbr\":\"USA\","
@"\"region_name\":\"Texas\","
@"\"start_time\":\"2015-04-27 08:00:00\","
@"\"tz_id\":null,"
@"\"description\":\" <p>Congratulations! As a financial planning professional you have acknowledged that planning for today&#39;s affluent is beginning to migrate from the &quot;accumulation&quot; phase of their lives onto now what is being termed the &quot;distribution&quot; or &quot;income phase of their lives. Some say timing is everything, we would like to say that for most clients it&#39;s when the right advisor knocks on their door.<p>Congratulations on deciding to take your practice to the next level in your effort to become a nationally recognized National Social Security Advisor (NSSA). This certification is attained by completing an eight (8) hour certified course and taking a proctored test of 75 questions. Successful advisors must score a minimum of 70% to become certified as a National Social Security Advisor.<p>Your Instructors for this course shall be:<p><strong>DEREK MISER</strong><p>Derek Miser has almost 20 years of experience in the financial planning and wealth management industry. Derek specializes in public speaking events and conducting financial workshop for those seeking to optimize their retirement income. He conducts NSSA certification classes in over 35 cities throughout the US.<p>Class Schedule: 8 am to 4:30 pm<p>The schedule will include two 15 minute breaks and a 60 minute break for lunch.<p>You may download your classroom materials on our website at www.b-tax-free.com at no charge or you may purchase them the day of the event from your instructor $15.00.<p>Class Tuition: $599</p></p></p></p></p></p></p></p></p>\","
@"\"modified\":\"2015-01-06 09:48:41\","
@"\"venue_display\":\"1\","
@"\"tz_country\":null,"
@"\"performers\":null,"
@"\"title\":\"National Social Security Advisor (NSSA)Workshop --Houston, TX\","
@"\"venue_address\":\"18700 John F. Kennedy Blvd\","
@"\"geocode_type\":\"EVDB Geocoder\","
@"\"tz_olson_path\":null,"
@"\"recur_string\":null,"
@"\"calendars\":null,"
@"\"owner\":\"evdb\","
@"\"going\":null,"
@"\"country_abbr2\":\"US\","
@"\"image\":{"
@"\"medium\":{"
@"\"width\":\"128\","
@"\"url\":\"http://s1.evcdn.com/store/skin/no_image/categories/128x128/other.jpg\","
@"\"height\":\"128\""
@"},"
@"\"thumb\":{"
@"\"width\":\"48\","
@"\"url\":\"http://s1.evcdn.com/store/skin/no_image/categories/48x48/other.jpg\","
@"\"height\":\"48\""
@"}"
@"},"
@"\"created\":\"2015-01-06 09:48:41\","
@"\"venue_id\":\"V0-001-005816585-7\","
@"\"tz_city\":null,"
@"\"stop_time\":\"2016-02-16 16:30:00\","
@"\"venue_name\":\"Marriot Houston Intercontinental at IAH\","
@"\"venue_url\":\"http://houston.eventful.com/venues/marriot-houston-intercontinental-at-iah-/V0-001-005816585-7?utm_source=apis&utm_medium=apim&utm_campaign=apic\""
@"}"
@"]"
@"}"
@"}";

static NSString *notJsonString = @"Not JSON";


@interface EventBuilderTests : XCTestCase{
    NSArray *events;
    Event *event;
    //EventBuilder *eventBuilder;
}

@end

@implementation EventBuilderTests

- (void)setUp {
    [super setUp];
    NSData *eventData = [eventJSON dataUsingEncoding:NSUTF8StringEncoding];
    events = [EventBuilder eventsFromJSON:eventData];
    event = [events objectAtIndex:0];
    
}

- (void)tearDown {
    events = nil;
    
    [super tearDown];
}

- (void)testEventBuilderCantParseNil {
    XCTAssertThrows([EventBuilder eventsFromJSON:nil], @"EventBuilder won't parse nil event");
}

- (void)testTotalNumberOfEvents {
    XCTAssertEqual([events count], 1);
}

- (void)testNilReturnedWhenReceivedNonJsonData {
    XCTAssertNil([EventBuilder eventsFromJSON:[notJsonString dataUsingEncoding:NSUTF8StringEncoding]], @"Event builder will ony parse JSON data");
}

- (void)testEventProperties{
    XCTAssertEqualObjects(event.title, @"National Social Security Advisor (NSSA)Workshop --Houston, TX");
    
    XCTAssertNotNil(event.startDate);
    XCTAssertNotNil(event.stopDate);
    XCTAssertEqualObjects(event.latitude, @"29.9865720");
    XCTAssertEqualObjects(event.longitude, @"-95.3417160");
    XCTAssertEqualObjects(event.venueName, @"Marriot Houston Intercontinental at IAH");
    XCTAssertEqualObjects(event.venueAddress, @"18700 John F. Kennedy Blvd");
    XCTAssertEqualObjects([event.eventURL absoluteString], @"http://houston.eventful.com/events/national-social-security-advisor-nssaworkshop-ho-/E0-001-079207392-0?utm_source=apis&utm_medium=apim&utm_campaign=apic");
    XCTAssertEqualObjects([[event.images objectForKey:@"thumb"] objectForKey:@"url"], @"http://s1.evcdn.com/store/skin/no_image/categories/48x48/other.jpg");
    
    XCTAssertEqualObjects([[event.images objectForKey:@"medium"] objectForKey:@"url"], @"http://s1.evcdn.com/store/skin/no_image/categories/128x128/other.jpg");

}

@end


