//
//  EventRequester.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColliEventsRequester.h"

@interface EventsRequester : ColliEventsRequester

- (NSURL *)urlToFetch;
- (NSURLSessionDataTask *)currentDataTask;

@end
