//
//  EventRequester.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "EventsRequester.h"

@implementation EventsRequester

- (NSURL *)urlToFetch{
    return fetchingURL;
}


- (NSURLSessionDataTask *)currentDataTask{
    return dataTask;
}

@end
