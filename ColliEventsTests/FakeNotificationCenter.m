//
//  FakeNotificationCenter.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-26.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "FakeNotificationCenter.h"

@implementation FakeNotificationCenter{
    NSMutableDictionary *observers;
    NSMutableArray *notifications;
}

- (id) init{
    self = [super init];
    if(self){
        observers = [NSMutableDictionary new];
        notifications = [NSMutableArray new];
    }
    
    return self;
}

- (void) addObserver: (id)observer selector:(SEL)selector name:(NSString *)aName object:(id)anObject{
    [observers setObject:observer forKey:aName];
}


- (void) removeObserver: (id)observer{
    NSArray *keys = [observers allKeys];
    for (NSString *k in keys){
        id obj = [observers objectForKey:k];
        if ([obj isEqual:observer]) {
            [observers removeObjectForKey:k];
        }
    }
    
}

- (void) postNotification: (NSNotification *)notification{
    [notifications addObject:notification];
}

- (void) removeObserver: (id)observer aName: (NSString *)aName object:(id)anObject{
    [self removeObserver:observer];
}

- (BOOL) hasObject: (id)observer forNotification: (NSString *) aName{
    return [[observers objectForKey:aName] isEqual:observer];
}


- (BOOL) didReceiveNotification: (NSString *)aName fromObject: (id)anObject{
    for (NSNotification *notification in notifications){
        if ([[notification name] isEqualToString:aName] && [[notification object] isEqual:anObject]) {
            return YES;
        }
    }
    
    return NO;
    
}

@end
