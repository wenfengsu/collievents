//
//  EventsSearchViewModelTest.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-22.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ColliEventsSearchViewModel.h"


// we need to expose the private methods for test.
@interface ColliEventsSearchViewModel (Test)

-(SearchCriterias *)getSearchCriterias;
-(void)resetSearchCriterias;
-(NSDate *) convertDateFromStartDay:(NSString *)day month:(NSString *)month year:(NSString *)year;

@end


@interface EventsSearchViewModelTests : XCTestCase{
    
    ColliEventsSearchViewModel *searchViewModel;
}

@end

@implementation EventsSearchViewModelTests

- (void)setUp {
    [super setUp];
    searchViewModel = [[ColliEventsSearchViewModel alloc] init];
}

- (void)tearDown {
    [super tearDown];
    [searchViewModel resetSearchCriterias];
    
}

-(void)testViewModelNotNil{
    XCTAssertNotNil(searchViewModel);
}


-(void)testValidStartDateInput{
    NSString *startDay = @"1";
    NSString *startMonth = @"1";
    NSString *startYear = @"2016";
    [searchViewModel validateStartDateEntered:startDay monthInput:startMonth yearInput:startYear];
    XCTAssertEqualObjects(searchViewModel.searchCriterias.startDate, [searchViewModel convertDateFromStartDay:startDay month:startMonth year:startYear], @"Start is valid");
    
}

-(void)testValidEndDateInput{
    NSString *endDay = @"2";
    NSString *endMonth = @"1";
    NSString *endYear = @"2016";
    [searchViewModel validateEndDateEndtered:endDay monthInput:endMonth yearInput:endYear];
    XCTAssertEqualObjects(searchViewModel.searchCriterias.endDate, [searchViewModel convertDateFromStartDay:endDay month:endMonth year:endYear]);
    
}

-(void)testInvalidStartDateInput{
    NSString *startDay = @"01";
    NSString *startMonth = @"20";
    NSString *startYear = @"2016";
    XCTAssertFalse([searchViewModel validateStartDateEntered:startDay monthInput:startMonth yearInput:startYear]);
    
}

-(void)testInvalidEndDateInput{
    NSString *endDay = @"A";
    NSString *endMonth = @"0";
    NSString *endYear = @"2016";
    XCTAssertFalse([searchViewModel validateStartDateEntered:endDay monthInput:endMonth yearInput:endYear]);
}


-(void)testStartDateGreaterThenEndDate{
    
    NSString *startDay = @"10";
    NSString *startMonth = @"02";
    NSString *startYear = @"2016";
    
    NSString *endDay = @"01";
    NSString *endMonth = @"01";
    NSString *endYear = @"2016";
    
    NSDate *startDate = [searchViewModel convertDateFromStartDay:startDay month:startMonth year:startYear];
    
    // one day after the startDate
    NSDate *actualEndDate = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:startDate];
    
    [searchViewModel validateStartDateEntered:startDay monthInput:startMonth yearInput:startYear];
    
    // we set a invalid endDate, this endDate will be ignored by the validator
    [searchViewModel validateEndDateEndtered:endDay monthInput:endMonth yearInput:endYear];
    XCTAssertEqualObjects(searchViewModel.searchCriterias.endDate, actualEndDate, @"actual end date is a day from start date");
}

-(void)testCoreDataDefaultSearchCriterias{
    
    SearchCriterias *searchCriterias = [searchViewModel getSearchCriterias];
    XCTAssertNotNil(searchCriterias);
    
    NSDateFormatter *df=[[NSDateFormatter alloc]init];
    [df setDateFormat:@"dd/MM/YYYY"];
    NSDate *startDate = [NSDate date];
    XCTAssertEqualObjects([df stringFromDate:searchCriterias.startDate], [df stringFromDate:startDate]);
    
    NSDate *endDate = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:startDate];
    XCTAssertEqualObjects([df stringFromDate:searchCriterias.endDate], [df stringFromDate:endDate]);
    
    NSString *category = @"Music";
    XCTAssertEqualObjects(searchCriterias.category, category);
    
    NSString *address = @"";
    XCTAssertEqualObjects(searchCriterias.address, address);

    NSNumber *radius = @1;
    XCTAssertEqualObjects(searchCriterias.radius, radius);
    
}


    



@end
