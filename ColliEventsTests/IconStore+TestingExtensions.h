//
//  IconStore+TestingExtensions.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-26.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "IconStore.h"

@interface IconStore (TestingExtensions)

- (void) setData: (NSData *)data forLocation: (NSString *)location;
- (NSUInteger)dataCacheSize;
- (NSDictionary *) requesters;
- (NSNotificationCenter *)notificationCenter;

@end
