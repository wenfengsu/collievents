//
//  ColliEventsRequesterTest.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-21.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "EventsRequester.h"
#import "EventBuilder.h"



@interface ColliEventsRequesterTests : XCTestCase{
    EventsRequester *requester;
    NSData *receivedData;
    NSURLSession *session;
    
}

@end

@implementation ColliEventsRequesterTests

- (void)setUp {
    [super setUp];
    
    requester = [EventsRequester new];
    receivedData = [@"Result" dataUsingEncoding:NSUTF8StringEncoding];
    session = [NSURLSession sharedSession];
}

- (void)tearDown {
    [requester cancelDataTask];
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSearchEventURL{
    
    // the generated URL must match the test one.
    [requester searchEventWithParamers:@{@"c":@"music", @"within": @"10"} errorHandler:nil successHanlder:nil];
    XCTAssertEqualObjects([[requester urlToFetch] absoluteString], @"http://api.eventful.com/json/events/search?app_key=hmCJHTTp2pqXHcsj&c=music&within=10");
}


- (void)testZeroEventReturned{
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"data accuracy test"];
    [requester searchEventWithParamers:nil errorHandler:^(NSError *error) {
        
        XCTAssertNil(error, @"dataTaskWithURL error %@", error);
        
    } successHanlder:^(NSData *data) {
        NSArray *events  = [EventBuilder eventsFromJSON:data];
        //This request only has 2 records
        XCTAssertEqual(events.count, (NSUInteger)0, @"wrong events size returned");
        [expectation fulfill];
        
    }];
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
    
}


- (void)testDataTaskAccuracy{
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"data accuracy test"];
    [requester searchEventWithParamers:@{@"date": @"2012042500-2012042700",@"c":@"music"} errorHandler:^(NSError *error) {
        
        XCTAssertNil(error, @"dataTaskWithURL error %@", error);
    
    } successHanlder:^(NSData *data) {
        NSArray *events  = [EventBuilder eventsFromJSON:data];
        //This request only has 2 records
        XCTAssertEqual(events.count, (NSUInteger)2, @"wrong events size returned");
        [expectation fulfill];

    }];
    
    [self waitForExpectationsWithTimeout:10.0 handler:nil];
    
}


- (void)testStartingNewSearchShouldKillOldConnection {
    [requester searchEventWithParamers:@{@"c":@"music", @"within": @"10"} errorHandler:nil successHanlder:nil];
    NSURLSessionDataTask *dataTaskOld = [requester currentDataTask];
    [requester searchEventWithParamers:@{@"c":@"music", @"within": @"20"} errorHandler:nil successHanlder:nil];
    XCTAssertFalse([dataTaskOld isEqual:[requester currentDataTask]], @"the requester need to restart a new data task");
   
}



@end
