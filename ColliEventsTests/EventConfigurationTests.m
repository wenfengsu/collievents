//
//  EventConfigurationTests.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-26.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ColliEventsConfigurator.h"
#import "ColliEventsManager.h"
#import "ColliEventsRequester.h"
#import "IconStore.h"
#import "IconStore+TestingExtensions.h"

@interface EventConfigurationTests : XCTestCase

@end

@implementation EventConfigurationTests{
    ColliEventsConfigurator *configurator;
}

- (void)setUp {
    [super setUp];
    configurator = [ColliEventsConfigurator new];
    
}

- (void)tearDown {
    configurator = nil;
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testEventManagerFromConfigurator{
    ColliEventsManager *manager = [configurator colliEventsManager];
    XCTAssertNotNil(manager, @"The event manager should be created at this point");
    XCTAssertNotNil(manager.eventsRequester, @"the event requester should be created at this point");
    XCTAssertEqualObjects(manager.eventsRequester.delegate, manager, @"the requester's delegate should be the manager itself");
}

- (void)testIconStoreFromEventManager{
    IconStore *store1 = [configurator iconStore];
    IconStore *store2 = [configurator iconStore];
    XCTAssertEqualObjects(store1, store2, @"the icon store should be always the same one.");
}

- (void)testNotificationCenterOfIconStore{
    IconStore *store = [configurator iconStore];
    XCTAssertEqualObjects([store notificationCenter], [NSNotificationCenter defaultCenter], @"iconstore is started with default noficiation center");
}

@end
