//
//  IconStoreTests.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-26.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FakeNotificationCenter.h"
#import "IconStore.h"
#import "IconStore+TestingExtensions.h"

@interface IconStoreTests : XCTestCase{
    FakeNotificationCenter *notificationCenter;
    IconStore *iconStore;
    NSData *fakeData;
    NSString *fakeLocation;
    NSString *abcLocation;
}


@end

@implementation IconStoreTests

- (void)setUp {
    [super setUp];
    
    notificationCenter = [FakeNotificationCenter new];
    iconStore = [IconStore new];
    fakeData = [@"fake image data" dataUsingEncoding:NSUTF8StringEncoding];
    fakeLocation = @"http://fakesite.com/event/fakeImage.jpg";
    [iconStore setData:fakeData forLocation:fakeLocation];
    abcLocation = @"http://someother.com/event/abc.jpg";
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testDataInCacahe{
    NSData *data = [iconStore dataForURL:[NSURL URLWithString:fakeLocation]];
    XCTAssertEqualObjects(data, fakeData, @"the data should be already in the cache");
}


#pragma mark - Test memory warning
- (void)testLowMemoryWarning{
    [iconStore didReceiveMemoryWarning:nil];
    XCTAssertEqual([iconStore dataCacheSize], (NSUInteger)0, @"cache should be cleaned up");
}

- (void) testIconStoreSubscribesToLowMemoryNotification{
    
    [iconStore startMemoryWarningNotificationWithNotificationCeter:(NSNotificationCenter *)notificationCenter];
    XCTAssertTrue([notificationCenter hasObject:iconStore forNotification:UIApplicationDidReceiveMemoryWarningNotification], @"icon store should have registered for low memory notification");
}


#pragma mark - Test reqeuster retrieving data from IconStore
-(void) testIconStoreRetrievedDataFromRequester{
    NSURL *abcUrl = [NSURL URLWithString:abcLocation];
    [iconStore iconRequestDidReceivedData:fakeData fromURL:abcUrl];
    XCTAssertEqualObjects([iconStore dataForURL:[NSURL URLWithString:abcLocation]], fakeData, @"the store should save the new data for the new url");
}

-(void) testCacaheMissCreatesNewReqeuster{
    // the iconstore won't receive any data from the URL, so the requesters should still have the requester in it.
    [iconStore dataForURL:[NSURL URLWithString:abcLocation]];
    XCTAssertNotNil([[iconStore requesters] objectForKey:abcLocation], @"icon store tries to fetch data from a fake url");
}

#pragma mark - IconStore Notification test
- (void) testIconStoreSendsUpdateNotificationWhenDataReceived{
    [iconStore startMemoryWarningNotificationWithNotificationCeter:(NSNotificationCenter *)notificationCenter];
    [iconStore iconRequestDidReceivedData:fakeData fromURL:[NSURL URLWithString:abcLocation]];
    XCTAssertTrue([notificationCenter didReceiveNotification:IconStoreDidUpdateContentNotification fromObject:iconStore], @"notify others when data receive");
    
}

#pragma mark - IconStore Requesters test
- (void) testIconStoreRemoveRequesterAfterDataReceived{
    [iconStore dataForURL:[NSURL URLWithString:fakeLocation]];
    [iconStore iconRequestDidReceivedData:fakeData fromURL:[NSURL URLWithString:abcLocation]];
    XCTAssertNil([[iconStore requesters] objectForKey:abcLocation], @"icon store should remove the reqeuster");
}


-(void) testIconStoreRemoveRequesterAfterDataRequestFailed{
    NSURL *url = [NSURL URLWithString:fakeLocation];
    [iconStore dataForURL:url];
    [iconStore iconRequestDidFailedWithError:nil fromURL:url];
    XCTAssertNil([[iconStore requesters] objectForKey:fakeLocation], @"the requester should be removed on failed request");
}

#pragma mark - IconStore failed/Nil test
- (void) testIconStoreCacheOnFailedRequest{
    NSUInteger cacaheSize = [iconStore dataCacheSize];
    NSURL *url = [NSURL URLWithString:fakeLocation];
    [iconStore dataForURL:url];
    [iconStore iconRequestDidFailedWithError:nil fromURL:url];
    XCTAssertEqual([iconStore dataCacheSize], cacaheSize, @"no data should be cached on failed request");
}

-(void) testNil{
    XCTAssertNil([iconStore dataForURL:nil], @"when url is nil, nil should be returned");
}
@end
