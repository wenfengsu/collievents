//
//  IconStore+TestingExtensions.m
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-26.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import "IconStore+TestingExtensions.h"

@implementation IconStore (TestingExtensions)

- (void) setData: (NSData *)data forLocation: (NSString *)location{
    [dataCache setObject:data forKey:location];
}


- (NSUInteger)dataCacheSize{
    return [[dataCache allKeys] count];
}
- (NSDictionary *) requesters{
    return requesters;
    
}

- (NSNotificationCenter *)notificationCenter{
    return notificationCenter;
}




@end
