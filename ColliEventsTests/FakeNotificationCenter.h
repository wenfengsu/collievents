//
//  FakeNotificationCenter.h
//  ColliEvents
//
//  Created by Edmund Su on 2016-04-26.
//  Copyright © 2016 Edmund Su. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FakeNotificationCenter : NSObject

- (void) addObserver: (id)observer selector:(SEL)selector name:(NSString *)aName object:(id)anObject;
- (void) removeObserver: (id)observer;
- (void) postNotification: (NSNotification *)notification;
- (void) removeObserver: (id)observer aName: (NSString *)aName object:(id)anObject;
- (BOOL) hasObject: (id)observer forNotification: (NSString *) aName;
- (BOOL) didReceiveNotification: (NSString *)aName fromObject: (id)anObject;

@end
